!!! Info for the CarbonTracker data assimilation system

datadir         : /home/awoude/ffdas/RINGO/Data

! list of all observation sites
obs.input.id   : obsfiles.csv
! number of observation sites included; number of species included and to be used in inversion
obs.input.nr   : 6
obs.spec.nr    : 2
obs.spec.name  : CO2,CO
! number of emission categories defined in the emission model
obs.cat.nr     : 14
! For Rdam obs
obs.sites.rc        : ${datadir}/sites_weights.rc
! number of parameters
nparameters     : 22
! set fixed seed for random number generator, or use 0 if you want to use any random seed
random.seed     : 4385
!file with prior estimate of scaling factors (statevector) and covariances
emis.pparam     : param_values.nc
ff.covariance   : covariances2.nc
!file with emission model parameter values
emis.paramfiles  : emission_factors

emis.ncountries : 10
countries       : AUS; BEL; CZE; FRA; DEU; LUX; NED; POL; CHE; GBR

! switch (1=on/0=off) and input data for background CO2 and CO concentrations
obs.bgswitch    : 1
obs.background  : ${datadir}/background.nc

! input data for emission model
emis.input.spatial : spatial_data.nc
emis.input.tempobs : time_profiles_stations.nc
emis.input.tempprior : time_profiles_stations.nc

! Area of the gridboxes
area.file :  /home/awoude/ffdas/RINGO/Data/area.nc

! overwrite existing prior/ensemble emission files + pseudo-data (0: keep existing files; 1: create new files)
run.emisflag            : 0
run.emisflagens         : 1
run.obsflag             : 0

! back trajectory time of STILT footprints, also applied to OPS (in hours)
run.backtime            : 24

! choose propagation scheme:
! 1: no propagation, start each cycle with the same prior parameter values and covariance matrix
! 2: propagation of optimized parameter values, but not of the covariance matrix
! 3: propagation of both optimized parameter values and covariance matrix
run.propscheme          : 3
